import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Patient } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class PatientService {


  constructor(private http: HttpClient) {
   
   }

   public getJSONPatient(): Observable<any> {
     return this.http.get("../assets/patients/patients.json");
   }

   public getJSONSummary(i): Observable<any>{
    return this.http.get('../assets/patientsSummary/'+i+'/summary.json');
  }

  public getJSONActivities(): Observable<any> {
    return this.http.get("../assets/definitions/activities.json");
  }

  public getJSONFirebase(): Observable<any> {
    return this.http.get("https://healthalert-b1ec7.firebaseio.com/posts.json/");
  }

  public postJSONPax(newPatient: Patient): Observable<Patient> {
    return this.http.post<Patient>("https://healthalert-b1ec7.firebaseio.com/posts.json", newPatient)
  }

}
