import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { PaxInRoomComponent } from '../pax-in-room/pax-in-room.component';
import { NgbProgressbarConfig, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { 
  ChartComponent, 
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart 
} from "ng-apexcharts";


export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
  providers: [NgbProgressbarConfig]
})
export class RoomComponent implements OnInit {
  @ViewChild("chart") chart: ChartComponent;
  
  @Input() rooms: any;
  @Input() roomsProgressValue: any;
  @Input() totDuration: any;
  @Input() maxDuration: number;

  public chartOptions: Partial<ChartOptions>;
  closeResult = '';
  indexForModal = 0;

  paxForChart = [];
  

  constructor(
    public config: NgbProgressbarConfig,
    private modalService: NgbModal
  ) {
    
    //PROGRESS BAR CONFIG
    config.max = this.maxDuration;
    config.striped = true;
    config.animated = true;
    config.height = '20px';


   }

  ngOnInit(): void {
    
  }

  open(content, x) {
    this.paxForChart = [];
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.indexForModal = x;
    this.rooms[x].patients.map((pax) => {
      
      this.paxForChart.push(pax)
    })
    
    this.getChart()
  }

  getChart(){
    let chartLabel = []
    let chartSeries = []
    this.paxForChart.filter(patient => {
      chartLabel.push(patient.activity)
      chartSeries.push(patient.duration)
    })
    this.chartOptions = {
      series: chartSeries,
      chart: {
        width: 380,
        type: "pie"
      },
      labels: chartLabel,
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
