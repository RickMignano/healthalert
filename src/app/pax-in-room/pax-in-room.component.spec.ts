import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaxInRoomComponent } from './pax-in-room.component';

describe('PaxInRoomComponent', () => {
  let component: PaxInRoomComponent;
  let fixture: ComponentFixture<PaxInRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaxInRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaxInRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
