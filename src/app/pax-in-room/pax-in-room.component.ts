import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service'
import { Patient, Room } from '../interfaces';
import { map } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner"; 
import { NgbProgressbarConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pax-in-room',
  templateUrl: './pax-in-room.component.html',
  styleUrls: ['./pax-in-room.component.scss'],
  providers: [NgbProgressbarConfig]
})
export class PaxInRoomComponent implements OnInit {

  patients : Patient[] = [];
  firstPatients : Patient[] = [];
  patientsOrdered: Patient[] = [];
  rooms : Room[] = []
  
  maxDuration = 0;
  totDuration = [];
  today = new Date();

  roomsProgressValue = [];
  

  constructor(
    private patientService: PatientService, 
    private spinnerService: NgxSpinnerService,
    public config: NgbProgressbarConfig
    ) {
      //PROGRESS BAR CONFIG
      config.max = this.maxDuration;
      config.striped = true;
      config.animated = true;
      config.height = '20px';
     }
  
  //Controllo se su firebase ho record di pazienti
  ngOnInit(): void {
    this.patientService.getJSONFirebase()
    .pipe(map(responseData => {
      this.spinnerService.show()
      for(const key in responseData) {
        if(responseData.hasOwnProperty(key)) {
          this.patients.push({ ...responseData[key], id: key})
        }
      }
      return this.patients;
    })
    )
    .subscribe(reqData => {
      return reqData;
    })
    .add(() => {
      let arrayMaxDur = []
        for(let i in this.patients) {
          arrayMaxDur[i] = (this.patients[i].duration)
        }
        this.maxDuration = Math.max(...arrayMaxDur)
        }).add(() => {
          this.initFirstPatients()
        })
        .add(() => {
          this.initFirstRooms()
        })
        .add(() => {
          //Ordino i pazienti per la durata dell'attività di svolgere
          this.patientsOrdered = (this.patients.sort((a,b) => (a.duration > b.duration) ? 1 : -1))
          this.spinnerService.hide()
        })
      }
  //Prendo i pazienti con durata massima consentita e li rimuovo dall'array pazienti
  //Questo perché li inserirò nelle prime stanze con durata massima consentita
  initFirstPatients() {
    this.firstPatients = this.patients.filter((patient, index) => { 
      if(patient.duration === this.maxDuration) {
          return this.patients.splice(index, 1)
      }
    })
  }
  //Inizializzo le prime stanze con i primi pazienti
  initFirstRooms() {
    let tempRoomDuration = 0;
    let tempFirstRoom: any;
    let tempPatient = [];
    this.firstPatients.map((patient, index) => {
      
      if(tempRoomDuration <= this.maxDuration) {
        tempPatient.push(patient)
      }

      tempFirstRoom = {id: index, name: 'Room ' + (index+1), patients: tempPatient, maxDuration: this.maxDuration};
      this.rooms.push(tempFirstRoom);
      tempPatient = [];
      tempRoomDuration = 0;

    })
  }
  
  //Algoritmo di riempimento stanze
  fillRooms() {
    this.rooms = [];
    this.initFirstRooms();
    this.spinnerService.show()
    let tempRoomDuration = 0;
    let tempPatients = [];
    let tempRoom;
    //Scorro i pazienti ordinati
      this.patientsOrdered.find((patient) => {
        if(patient.duration<this.maxDuration - tempRoomDuration) {
          tempPatients.push(patient)
          tempRoomDuration += patient.duration
        }
        else {
          this.patientsOrdered.reverse()
          tempRoom = {id: this.rooms.length+1, name: 'Room ' + (this.rooms.length+1), patients: tempPatients, maxDuration: this.maxDuration}
          this.rooms.push(tempRoom)
          tempRoomDuration = patient.duration
          tempPatients = []
          tempPatients.push(patient)
        }
      })
     if (tempPatients.length != 0) {
        tempRoom = {id: this.rooms.length+1, name: 'Room ' + (this.rooms.length+1), patients: tempPatients, maxDuration: this.maxDuration}
        this.rooms.push(tempRoom)
        this.patientsOrdered.reverse()
     }

    this.totDurationForRoom()
    setTimeout(()=> {
      this.spinnerService.hide()
    }, 200)
  }


  //Calcolo la durata totale per stanza
  totDurationForRoom() {
    let tempPatient;
    let patientTime = [];
    this.rooms.map((room, i) => {
      tempPatient = room.patients;
      patientTime = [];
      tempPatient.map((patient) => {
        patientTime.push(patient.duration)
      })
      this.totDuration.push(patientTime.reduce((a, b) => a +b))
    })
    this.progressBarPercentage()
  }

  progressBarPercentage() {
    this.totDuration.map(roomDuration => {
      this.roomsProgressValue.push((roomDuration/this.maxDuration) * 100)
    })
  }


}
