import { Component, OnInit, PipeTransform } from '@angular/core';
import { PatientService } from '../patient.service';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner"; 

interface Patient {
  id: number;
  name: string;
  gender: string;
  birthDate: string;
  age: number;
  heightCm: number;
  weightKg: number;
  bmi: number;
  status: string;
  activities: Array<any>;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DecimalPipe]
})
export class DashboardComponent implements OnInit {

  patients$: Observable<Patient[]>;
  filter = new FormControl('');

  PATIENTS: Patient[] = []

  //Creo array vuoti per poter unire i vari JSON che importo dai service
  patientActivities = [];
  activityIntensity = [];

  //Creo array per spiegare
  explanation = [
    {
      title: 'All patients aged between 20 and 40',
      content: 'Please choose one particular case for the analysis'
    }
  ]
  navbarOpen = false;


  
  //Richiamo il service nel costruttore
  constructor(
    private patientService: PatientService, //Service con dati
    private pipe: DecimalPipe,
    private spinnerService: NgxSpinnerService
    ) { }

  ngOnInit(): void {
    //Importo la lista dei pazienti
      const promiseOne = this.patientService.getJSONPatient().toPromise()
      promiseOne.then(data => {
        this.spinnerService.show()
        for (let i = 0; i<data.length; i++) {
          this.PATIENTS.push(data[i]); 
          //Calcolo l'età del paziente
          if (this.PATIENTS[i].birthDate) {
            this.calculate_age(i)  
          }
          //Inizializzo lo status del paziente a tutti
          if (!this.PATIENTS[i].status) {
            this.PATIENTS[i].status = 'all';
          }

          this.patients$ = this.filter.valueChanges.pipe(
            startWith(''),
            map(text => this.search(text, this.pipe))
          );
        }
        
      }).then(()=> {
        //Per ciascun paziente importo le attività svolte
        for (let p = 1; p < this.PATIENTS.length + 1; p++) {
          const promiseTwo = this.patientService.getJSONSummary(p).toPromise()
          promiseTwo.then(
            dataPat => {
              this.patientActivities.push(dataPat)
              if(p==this.PATIENTS.length) {
                this.initActivities()
              }
            }
          )
        }
      })
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
  
  //Funzione helper per caclcolare l'età a partire dalla data di nascita
  calculate_age(i) { 
    var timeDiff = Math.abs(Date.now() - new Date(this.PATIENTS[i].birthDate).getTime());
    this.PATIENTS[i].age = (Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25));        
  }

  search(text: string, pipe: PipeTransform): Patient[] {
    return this.PATIENTS.filter(patient => {
      const term = text.toLowerCase();
      return patient.name.toLowerCase().includes(term)
          || patient.gender.includes(term)
          || patient.birthDate.includes(term)
          || pipe.transform(patient.age).includes(term)
          || this.PATIENTS.map(x=>x.activities.map(y=>y.activity)).join(' ').includes(term)
    });
  }


  //Importo la definizione di intensità di ogni attività e la preparo alla comparazione con le attività svolte
  initActivities() {
    const promiseThree = this.patientService.getJSONActivities().toPromise()
    promiseThree.then(dataAct => {
      for (let z = 0; z < dataAct.length; z++) {
        this.activityIntensity.push(dataAct[z])
        if(z == dataAct.length - 1) {
          this.compareActivities()
        }
      }
    })
  }
  //Creo un oggetto che contenga per ciascuna attività la relativa intensità
  compareActivities() {
    for (let i = 0; i< this.patientActivities.length; i++) {
      const activity = this.patientActivities[i].map(x=> Object.assign(x, this.activityIntensity.find(y=>y.activity == x.activity)))
      this.PATIENTS[i].activities = (activity)
    }
    this.spinnerService.hide()
  }
  
  setActivityRange(i, intens, min) {
    //Gestisco tramite aggiunta e rimozione classi, la visualizzazione delle descrizioni per ciascun criterio di ricerca
    //Faccio uso di ViewChild per selezionare l'oggetto interessato
    for(let x = 0; x < i; x++) {
      this.PATIENTS[x].status = "none"
      switch(intens) {
        case 'all':
          this.PATIENTS[x].status = "all"
          this.explanation = [
            {
              title: 'All patients aged between 20 and 40',
              content: ''
            }
          ]
          break;
        case 'moderate':
          for (let z = 0; z < this.PATIENTS[x].activities.length; z++) {
            if(this.PATIENTS[x].activities[z].intensity == intens && this.PATIENTS[x].activities[z].minutes >= min) {
              this.PATIENTS[x].status = "moderate" //in questo caso associo true al paziente che rispetta il criterio
            }  
            this.explanation = [
              {
                title: 'Moderate Activity',
                content: 'In this section will be shown patient with 150 minutes of moderate intensity activity'
              }
            ]
          }
          break;
        case 'vigorous':
          for (let z = 0; z < this.PATIENTS[x].activities.length; z++) {
            if(this.PATIENTS[x].activities[z].intensity == intens && this.PATIENTS[x].activities[z].minutes >= min) {
              this.PATIENTS[x].status = "vigorous" //in questo caso associo true al paziente che rispetta il criterio
            }  
          }
          this.explanation = [
            {
              title: 'Vigorous Activity',
              content: 'In this section will be shown patient with 75 minutes of vigorous intensity activity'
            }
          ]
          break;
        case 'mixable':
          for (let z = 0; z < this.PATIENTS[x].activities.length; z++) {
            if((this.PATIENTS[x].activities[z].intensity == "vigorous" && this.PATIENTS[x].activities[z].minutes >= 25) || (this.PATIENTS[x].activities[z].intensity == "moderate" && this.PATIENTS[x].activities[z].minutes >= 100)) {
              this.PATIENTS[x].status = "mixable"
            }  
          }
          this.explanation = [
            {
              title: 'Mixed Activity',
              content: 'In this section will be shown patient with a proportionate mix of moderate and vigorous activity (25 vig. / 100 mod.)'
            }
          ]
          break;
        default:
          this.PATIENTS[x].status = "none"
      }
    }
  }
  
  
}
