import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaxregistrationComponent } from './paxregistration.component';

describe('PaxregistrationComponent', () => {
  let component: PaxregistrationComponent;
  let fixture: ComponentFixture<PaxregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaxregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaxregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
