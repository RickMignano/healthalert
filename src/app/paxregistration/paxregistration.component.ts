import { Component, OnInit,  PipeTransform } from '@angular/core';
import { PatientService } from '../patient.service';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { Patient } from '../interfaces';

@Component({
  selector: 'app-paxregistration',
  templateUrl: './paxregistration.component.html',
  styleUrls: ['./paxregistration.component.scss'],
  providers: [DecimalPipe]
})
export class PaxregistrationComponent implements OnInit {

  rows = [""]
  newPatient : Patient[] = []

  patientName = "";
  patientSurname = "";
  patientActivity = "";
  patientDuration = 0;
  patients : Patient[] = [];
  patients$: Observable<Patient[]>;
  filter = new FormControl('');
  activities = [];

  constructor(private patientService: PatientService, private http: HttpClient, private pipe: DecimalPipe) { }

  ngOnInit(): void {
    this.getPatients();
    this.getActivities();
  }
  //Aggiungo pazienti in attesa di essere caricati sul Database
  addPendingPax() {
    this.newPatient.push({
      id: this.newPatient.length,
      name: this.patientName,
      surname: this.patientSurname,
      activity: this.patientActivity,
      duration: this.patientDuration
    })
    this.emptyField()
  }
  //Ottengo le attività per inserirle nella select
  getActivities() {
    this.patientService.getJSONActivities()
    .pipe(map(dataAct => {
      for (let key in dataAct) {
        this.activities.push({ ...dataAct[key], id: key})
      }
      return this.activities;
    }))
    .subscribe(reqAct => {
      return reqAct
    })
  } 
  
  //Svuoto il campo dopo aver inserito il paziente in Pending
  emptyField() {
    this.patientName = "",
    this.patientSurname = "",
    this.patientActivity = "",
    this.patientDuration = 0
  }
  //Possibilità di rimuovere il paziente da pending
  deletePendPax(j) {
    this.newPatient.splice(j, 1)
  }

  //Carico i pazienti nel DB
  postPatients(){
    this.newPatient.map((newPatient) => {
      this.patientService.postJSONPax(newPatient).subscribe(data => {
        return data;
      })
      .add(() => {
        window.location.reload();
      })
    })
  }

  //Ottengo i pazienti dal DB
  getPatients(){
    this.patientService.getJSONFirebase()
    .pipe(map(responseData => {
      for(const key in responseData) {
        if(responseData.hasOwnProperty(key)) {
          this.patients.push({ ...responseData[key], id: key})
        }
      }
      return this.patients;
    })
    )
    .subscribe(reqData => {
      return reqData;
    })

    this.patients$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text, this.pipe))
    );
  }

  //Funzione di ricerca in tabella
  search(text: string, pipe: PipeTransform): Patient[] {
    return this.patients.filter(patient => {
      const term = text.toLowerCase();
      return patient.name.toLowerCase().includes(term)
          || patient.surname.toLowerCase().includes(term)
          || patient.activity.includes(term)
          || pipe.transform(patient.duration).includes(term)
    });
  }
}
