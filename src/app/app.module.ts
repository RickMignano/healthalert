import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from "ngx-spinner";
import { MouseHoverDirective } from './mouse-hover.directive';
import { PaxregistrationComponent } from './paxregistration/paxregistration.component';
import { PaxInRoomComponent } from './pax-in-room/pax-in-room.component';
import { RoomComponent } from './room/room.component';
import { NgApexchartsModule } from 'ng-apexcharts'; 


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    MouseHoverDirective,
    PaxregistrationComponent,
    PaxInRoomComponent,
    RoomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    NgApexchartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
