export interface Patient {
    id: number;
    name: string;
    surname: string;
    activity: string;
    duration: number;
}

export interface Room {
    id: number;
    name: string;
    patients: any;
    maxDuration: number;
}